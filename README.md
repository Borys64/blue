![Screenshot](./screenshot.png)

# Blue
An arc-like theme for openbox window borders. Works best with the arc widget theme.

![Screenshot](./screenshot2.png)